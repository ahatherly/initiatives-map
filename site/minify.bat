@ECHO OFF
REM ***********************************************************************
REM * Set the below to the path of the closure minifier jar file          *
REM * Available from: https://developers.google.com/closure/compiler/     *
REM ***********************************************************************

set CLOSURE_JAR=I:/Personal/Tools/closure-js-compiler-latest/compiler.jar

@ECHO ON
mkdir minified
java -jar %CLOSURE_JAR% --js loadJSON.js --js_output_file minified/loadJSON.js
java -jar %CLOSURE_JAR% --js ccg.js --js_output_file minified/ccg.js
java -jar %CLOSURE_JAR% --js filters.js --js_output_file minified/filters.js
java -jar %CLOSURE_JAR% --js mapFunctions.js --js_output_file minified/mapFunctions.js
copy map.html minified\map.html
copy data.json minified\data.json
pause