/*
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
// *******************************  Filtering *********************************************
var filterFieldList;
var filterFieldsAreArrays;
var filterFieldsColspans;
var filterRowFieldCounts;
var filterFieldCount;
var filterFieldValue;

function initialiseFilters() {
	filterFieldList = data['filterFieldList'];
	filterFieldsAreArrays = data['filterFieldsAreArrays'];
	filterFieldsColspans = data['filterFieldsColspans'];
	filterRowFieldCounts = data['filterRowFieldCounts'];
	filterFieldCount = filterFieldList.length;
	filterFieldValue = new Array();
	for (var n=0; n<filterFieldCount; n++) {
		filterFieldValue[n]="";
	}
}

// Returns true if the record should be shown, false if it should be hidden
function shouldBeShown(index, filterFieldValuesToApply) {
	var show = true; // Assume it should be shown unless one of the filers excludes it
	for (var n=0; n<filterFieldCount; n++) {
		if (filterFieldValuesToApply[n]!="") { // If the filter has a value..
			if (!filterFieldsAreArrays[n]) { // If the filter is not an array, check if it matches
				if (data['initiatives'][index][filterFieldList[n]] != filterFieldValuesToApply[n]) {
					show = false;
				}
			} else { // If it is an array, check if the filter value is in the array
				if (findArrayIndex(data['initiatives'][index][filterFieldList[n]], filterFieldValuesToApply[n])==-1) {
					show = false;
				}
			}
		}
	}
	return show;
}

// Build the filter drop-downs
function buildFilterDiv() {
   var content = "";
   content = content + "<div id='filter' style='margin: auto; width: 600px;'>";
   content = content +   "<table border='0' cellpadding='0' cellspacing='2'><tr>";
   var fieldsInRow = 0;
   var row = 0;
   for (var ff=0; ff<filterFieldList.length; ff++) {
   	   var fieldName = filterFieldList[ff];
   	   var fieldIsArray = filterFieldsAreArrays[ff];
   	   if (row < filterRowFieldCounts.length) {
		   if (fieldsInRow == filterRowFieldCounts[row]) {
			   content = content + "</tr><tr>";
			   row = row + 1;
			   fieldsInRow = 0;
		   }
   	   }
	   content = content +     "<td class='header rightText'>" + fieldName + ":</td><td colspan='" + filterFieldsColspans[ff] + "'>"
	   content = content +       "<select onchange='applyFilter(this,\"" + fieldName + "\"," + fieldIsArray + ");'>"   
	   var values = getValuesForFilterDropDown(fieldName, fieldIsArray);
	   for (var n=0; n<values.length; n++) {
		   content = content +       "<option " + getIsSelected(fieldName,values[n]['value']) + " value='" + values[n]['value'] + "'>"+ values[n]['value'] + " (" + values[n]['counter'] + ")</option>";
	   }
	   content = content +       "</select>";
	   content = content +     "</td>";
	   fieldsInRow = fieldsInRow + 1;
   }
   content = content +   "<td><a href='#' onclick='clearFilters();'>Clear</a></td></tr></table>";
   content = content +   "</table>";
   content = content + "</div>";
   return content;
}

// Returns "selected" if this value is the selected filter value for this field
function getIsSelected(field, value) {
	for (var n=0; n<filterFieldCount; n++) {
		if (filterFieldList[n] == field) {
			if (filterFieldValue[n] == value) {
				return "selected";
			} else {
				return "";
			}
		}
	}
	return "";
}

function clearFilters() {
	for (var n=0; n<filterFieldCount; n++) {
		filterFieldValue[n]="";
		initialise();
	}
}

function applyFilter(control, fieldName, isArray) {
	index = getFilterFieldIndex(fieldName);
	if (control.value=="Any") {
		filterFieldValue[index]="";
	} else {
		filterFieldValue[index]=control.value;
	}
	initialise();
}

function getFilterFieldIndex(fieldName) {
	var index = 0;
	while ((index<filterFieldCount) && (filterFieldList[index] != fieldName)) {
		index++;
	}
	return index;
}

function getValuesForFilterDropDown(fieldName, isArrayField) {
	var values = [ {'value':'Any', 'counter':0} ];
	// We need to find the unique values of the field, but only after applying all the other filters (except this one)
	index = getFilterFieldIndex(fieldName);
	filterFieldValuesTemp = filterFieldValue.slice(0); // Clone the array
	filterFieldValuesTemp[index]="";
	
	for (var index=0; index<data['initiatives'].length; index++) {
		if (shouldBeShown(index, filterFieldValuesTemp)) {
			values[0]['counter']=values[0]['counter']+1; // Increment counter for "All"
			var value = data['initiatives'][index][fieldName];
			if (!isArrayField) {
				var entryIndex=findObjectArrayIndex(values, 'value', value); 
				if (entryIndex==-1) {
					// Add to the drop-down
					values.push({'value':value,'counter':1});
				} else {
					// Increment the counter
					values[entryIndex]['counter']=values[entryIndex]['counter']+1;
				}
			} else {
				for (var i=0; i<value.length; i++) {
					var entryIndex=findObjectArrayIndex(values, 'value', value[i]);
					if (findArrayIndex(values, value[i])==-1) {
						if (entryIndex==-1) {
							// Add to the drop-down
							values.push({'value':value[i],'counter':1});
						} else {
							// Increment the counter
							values[entryIndex]['counter']=values[entryIndex]['counter']+1;
						}
					}
				}
			}
		}
	}
	return values;
}

function hideFilter() {
	var content = "<div style='width:100%; text-align:right;' class='more pointer' onclick='showFilter()'>Show Filter</div>"
	document.getElementById('showHideFilter').innerHTML=content;
	document.getElementById('filter').style.display = "none";        
}

function showFilter() {
	var content = "<div style='width:100%; text-align:right;' class='more pointer' onclick='hideFilter()'>Hide Filter</div>"
	document.getElementById('showHideFilter').innerHTML=content;
	document.getElementById('filter').style.display = "inline";
}

// ************************** Utility Functions *************************************

// Returns -1 if not in array. If in array, returns index
function findArrayIndex(array, value) {
	for (var n=0; n<array.length; n++) {
		if (array[n]==value) {
			return n;
		}
	}
	return -1;
}

// Returns -1 if not in the object array. If in the object array, returns index
function findObjectArrayIndex(objectArray, key, value) {
	for (var n=0; n<objectArray.length; n++) {
		if (objectArray[n][key]==value) {
			return n;
		}
	}
	return -1;
}

function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return unescape(pair[1]);
            }
        }
    }
