/*
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
var map;
var infoWindow = new google.maps.InfoWindow();
var padding = 0.01;
var minLat = -100;
var maxLat = 100;
var minLng = -100;
var maxLng = 100;

function initialise() {
      	
      	var myLatLng = new google.maps.LatLng(52.842595,-2.263184);
		var mapOptions = {
		  zoom: 7,
		  center: myLatLng,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		
		// Remove any previous label content
		for (var key in ccgData) {
			var ccg = ccgData[key];
			delete ccg['content'];
		}
		
		// Add polygons for each initiative
		//var haveRecentred = false;
		minLat = -100;
		maxLat = 100;
		minLng = -100;
		maxLng = 100;
		for (var i=0; i<data['initiatives'].length; i++) {
			if (shouldBeShown(i, filterFieldValue)) {
				var orgID = data['initiatives'][i]['org'];
				
				if (orgID.indexOf(",") !== -1) {
					orgIDList = orgID.split(",");
					for (var orgIndex=0; orgIndex<orgIDList.length; orgIndex++) {
						processCCG(orgIDList[orgIndex], i);
					}
				} else {
					processCCG(orgID, i);
				}
			}
		}
		
		// Now reposition the map and zoom level
		var bounds = new google.maps.LatLngBounds(new google.maps.LatLng(minLat-padding,minLng-padding), new google.maps.LatLng(maxLat+padding,maxLng+padding));
		map.fitBounds(bounds);
		
		// Now show the filters
		var content = buildFilterDiv();
    	document.getElementById('filter_div').innerHTML=content;
		
      }
      
      function processCCG(orgID, initiativeIndex) {
      	    var desc = ccgData[orgID]['desc'];
			// Keep a running total of the minimum lats and lngs, so we can reposition and zoop the map accordingly
			if ((ccgData[orgID]['minLat']<minLat) || (minLat==-100)) {	minLat=ccgData[orgID]['minLat'];	}
			if ((ccgData[orgID]['maxLat']>maxLat) || (maxLat==100)) {	maxLat=ccgData[orgID]['maxLat'];	}
			if ((ccgData[orgID]['minLng']<minLng) || (minLng==-100)) {	minLng=ccgData[orgID]['minLng'];	}
			if ((ccgData[orgID]['maxLng']>maxLng) || (maxLng==100)) {	maxLng=ccgData[orgID]['maxLng'];	}
			// Build the label value
			if (typeof ccgData[orgID]['content'] === "undefined") {
				ccgData[orgID]['content'] = "<div class='ccgLabel'><b>" + desc + "</b><br/><ul>";
			}
			ccgData[orgID]['content'] = ccgData[orgID]['content'] +
			"<li>Initiative: <a target='_blank' href='" + data['initiatives'][initiativeIndex]['url'] + "'>" + data['initiatives'][initiativeIndex]['name'] + "</a></li>";
			// Add the polygon to the map
			addPolygon(ccgData[orgID]['points'], ccgData[orgID]['content'], data['initiatives'].length, initiativeIndex);
      }
      
      function addPolygon(points, contentString, initiativeCount, initiativeIndex) {
      	  	// Construct the polygon.
      	  	colour = rainbow(initiativeCount, initiativeIndex);
			ccg = new google.maps.Polygon({
				paths: points,
				strokeColor: '#FF0000',
				strokeOpacity: 0.8,
				strokeWeight: 1,
				fillColor: colour,
				fillOpacity: 0.3
			});
			ccg.setMap(map);
			// Add a listener for the click event.
			google.maps.event.addListener(ccg, 'click', function(event) {
				infoWindow.close();
				var vertices = this.getPath();
				infoWindow.setPosition(event.latLng);
				infoWindow.setContent(contentString + "</ul></div>");
				infoWindow.open(map);
            });
      }
      
      function rainbow(numOfSteps, step) {
			// This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
			// Adam Cole, 2011-Sept-14
			// HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
			var r, g, b;
			var h = step / numOfSteps;
			var i = ~~(h * 6);
			var f = h * 6 - i;
			var q = 1 - f;
			switch(i % 6){
				case 0: r = 1, g = f, b = 0; break;
				case 1: r = q, g = 1, b = 0; break;
				case 2: r = 0, g = 1, b = f; break;
				case 3: r = 0, g = q, b = 1; break;
				case 4: r = f, g = 0, b = 1; break;
				case 5: r = 1, g = 0, b = q; break;
			}
			var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
			return (c);
	  }
