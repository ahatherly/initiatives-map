/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package uk.nhs.icase.converters;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import uk.nhs.icase.utils.FileLoader;
import uk.nhs.icase.utils.FileWriter;
import uk.nhs.icase.utils.XMLParser;

public class KMLtoPolygonConverter {

	public static void main(String[] args) throws SAXParseException, ParserConfigurationException, SAXException, IOException {
		if (args.length != 1) {
			System.out.println("Usage: java -cp target\\iCaseUtils.jar uk.nhs.icase.converters.KMLtoPolygonConverter \"PathToKMLFile\"");
		}
		String filename = args[0];
		System.out.println("Loading KML file: " + args[0]);
		String content = FileLoader.loadFile(filename);
		
		ByteArrayInputStream is = new ByteArrayInputStream(content.getBytes());
		Document doc = XMLParser.generateDOM(is, false);
		Element root = doc.getDocumentElement();
		
		System.out.println("Root node: " + root.getNodeName());
		
		/*
		 * Extract list of CCGs
		 */
		ArrayList<CCG> ccgList = new ArrayList<CCG>();
		NodeList ccgNodes = XMLParser.findNodesFromXPath("Folder/Placemark", root);
		for (int n=0; n<ccgNodes.getLength(); n++) {
			CCG ccg = new CCG();
			Element e = (Element)ccgNodes.item(n);
			ccg.name = e.getElementsByTagName("name").item(0).getTextContent();
			ccg.description = extractName(e.getElementsByTagName("description").item(0).getTextContent());
			
			System.out.println("Org: " + ccg.name + " - description: " + ccg.description);
			
			Element ccgPolygonElement = XMLParser.findNodeFromXPath("Polygon/outerBoundaryIs/LinearRing/coordinates", e);
			String ccg_polygon = null;
			if (ccgPolygonElement != null) {
				ccg_polygon = ccgPolygonElement.getTextContent();
			} else {
				// See if we have a multi-geometry
				ccgPolygonElement = XMLParser.findNodeFromXPath("MultiGeometry/Polygon/outerBoundaryIs/LinearRing/coordinates", e);
				if (ccgPolygonElement != null) {
					ccg_polygon = ccgPolygonElement.getTextContent();
				}
			}
			if (ccg_polygon != null) {
				if (ccg_polygon.contains("\n")) {
					String[] lines = ccg_polygon.split("\n");
					for (String line : lines) {
						LatLng point = extractLatLng(line);
						if (point != null) {
							ccg.points.add(point);
							// Check if this is the min or max latitude or longitude, to give us
							// the bounding box of the polygon. We will use this to re-size and re-position
							// the map around the visible polygons
							if ((Double.parseDouble(point.lat) < ccg.minLat) || (ccg.minLat == -100)) {
								ccg.minLat = Double.parseDouble(point.lat);
							}
							if ((Double.parseDouble(point.lat) > ccg.maxLat) || (ccg.maxLat == 100)) {
								ccg.maxLat = Double.parseDouble(point.lat);
							}
							if ((Double.parseDouble(point.lng) < ccg.minLng) || (ccg.minLng == -100)) {
								ccg.minLng = Double.parseDouble(point.lng);
							}
							if ((Double.parseDouble(point.lng) > ccg.maxLng) || (ccg.maxLng == 100)) {
								ccg.maxLng = Double.parseDouble(point.lng);
							}
						}
					}
					ccgList.add(ccg);
				} else {
					System.out.println(" - Error: No points found!");
				}
			} else {
				System.out.println(" - Error: No polygon found!");
			}
		}
		
		/*
		 * Write javascript
		 */
		StringBuilder javascript = new StringBuilder();
		javascript.append("var ccgList = [ ");
		boolean isFirstCCG = true;
		for (CCG ccg : ccgList) {
			if (!isFirstCCG) {
				javascript.append(",");
			}
			isFirstCCG = false;
			javascript.append("'").append(ccg.name).append("'");
		}
		javascript.append("];\n");
		
		javascript.append("var ccgData = {\n");
		isFirstCCG = true;
		for (CCG ccg : ccgList) {
			if (!isFirstCCG) {
				javascript.append(",");
			}
			isFirstCCG = false;
			javascript.append("  '").append(ccg.name).append("' :\n    { 'points':[ ");
			boolean isFirstPoint = true;
			for (LatLng point : ccg.points) {
				if (!isFirstPoint) {
					javascript.append(",");
				}
				isFirstPoint = false;
				javascript.append("new google.maps.LatLng(").append(point.lat).append(",").append(point.lng).append(")");
			}
			javascript.append("],\n");
			javascript.append("      'minLat':").append(ccg.minLat).append(",'maxLat':").append(ccg.maxLat).append(",");
			javascript.append("'minLng':").append(ccg.minLng).append(",'maxLng':").append(ccg.maxLng).append(",\n");
			javascript.append("      'desc':'").append(ccg.description).append("'}\n");
		}
		javascript.append("};\n");
		String directory = new File(filename).getParentFile().getAbsolutePath();
		String outFile = directory + "/ccg.js";
		FileWriter.writeFile(outFile, javascript.toString().getBytes());
		System.out.println("Javascript written to file: " + outFile);
	}
	
	private static String extractName(String html) {
		int idx = html.indexOf("CCGname</td><td>") + 16;
		int endidx = html.indexOf("</td></tr>", idx);
		return html.substring(idx, endidx);
	}

	private static LatLng extractLatLng(String line) {
		LatLng point = new LatLng();
		if (line.contains(",")) {
			String[] items = line.split(",");
			point.lng = items[0].trim();
			point.lat = items[1].trim();
			return point;
		} else {
			return null;
		}
	}
}
