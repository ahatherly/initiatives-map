/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package uk.nhs.icase.utils;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XMLParser {
	
	private static XPathFactory xPathfactory = XPathFactory.newInstance();
	
	public static Document generateDOM(InputStream xmlContent, boolean namespaceAware) throws ParserConfigurationException, SAXException, IOException, SAXParseException {
		Document result = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(namespaceAware);
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();
		result = builder.parse(xmlContent);
		return result;
	}
	
	public static NodeList findNodesFromXPath(String strXpath, Element parent) {
		try {
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(strXpath);
			NodeList list = (NodeList)expr.evaluate(parent, XPathConstants.NODESET);
			return list;
		} catch (XPathExpressionException e) {
			Logger.error("	Error evaluating XPath: " + strXpath, e);
		}
		return null;
	}
	
	public static Element findNodeFromXPath(String strXpath, Element parent) {
		try {
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(strXpath);
			Element result = (Element)expr.evaluate(parent, XPathConstants.NODE);
			return result;
		} catch (XPathExpressionException e) {
			Logger.error("	Error evaluating XPath: " + strXpath, e);
		}
		return null;
	}
}
