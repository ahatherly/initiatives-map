/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package uk.nhs.icase.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Simple Logging class - created to avoid unnecessary dependencies
 * on external libraries
 *
 * @author Michael Odling-Smee 
 *
 */
public class Logger { 
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
	private static final int PAD_WIDTH = 40;
	
	public static final int TRACE = 0;
	public static final int DEBUG = 10;
	public static final int INFO = 20;
	public static final int WARN = 30;
	public static final int ERROR = 40;
	public static final int FATAL = 50;
	
	private static int logLevel = INFO;
	
	static {
		logLevel = parseLogLevel(PropertyReader.getProperty("loglevel"));
	}
	
	public static void setLogLevel(int level) {
		logLevel = level;
	}
	
	private static final void log(int level, String message, Throwable t) {
		if (level >= logLevel) {
			SimpleDateFormat formatter = new SimpleDateFormat();
			Calendar CAL = Calendar.getInstance(); 
			String dateTime = formatter.format(CAL.getTime());
			String threadId = Thread.currentThread().getName();
			String logMessage = dateTime + " [" + threadId + "] [" + logLevelStr(level) + "] " + pad(getCallerInfo()) + message;
			if (t == null) {
				System.out.println(logMessage);
			} else {
				System.out.println(logMessage + ", " + t.getLocalizedMessage());
				t.printStackTrace(System.out);
			}
		}
	}
	
	private static String pad(String val) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int len = val.length();
		if (len<PAD_WIDTH) {
			try {
				bos.write(val.getBytes());
			} catch (IOException e) {}
			for (int n=val.length(); n<PAD_WIDTH; n++) {
				bos.write(' ');
			}
		}
		return bos.toString();
	}
	
	private static final int parseLogLevel(String logLevelStr) {
		if (logLevelStr != null) {
			if (logLevelStr.equalsIgnoreCase("TRACE")) {
				return TRACE;
			} else if (logLevelStr.equalsIgnoreCase("DEBUG")) {
				return DEBUG;
			}  else if (logLevelStr.equalsIgnoreCase("INFO")) {
				return INFO;
			}  else if (logLevelStr.equalsIgnoreCase("WARN")) {
				return WARN;
			}  else if (logLevelStr.equalsIgnoreCase("ERROR")) {
				return ERROR;
			}  else if (logLevelStr.equalsIgnoreCase("FATAL")) {
				return FATAL;
			}
		}
		//Default to INFO level
		return INFO;
	}
	
	private static final String logLevelStr(int level) {
		switch (level) {
			case TRACE:
				return "TRACE";
			case DEBUG:
				return "DEBUG";
			case INFO:
				return "INFO";
			case WARN:
				return "WARN";
			case ERROR:
				return "ERROR";
			case FATAL:
				return "FATAL";
			default:
				return null;
		}
	}
	
	public static final void trace(String message) {
		log(TRACE, message, null);
	}
	
	public static final void debug(String message) {
		log(DEBUG, message, null);
	}
	
	public static final void info(String message) {
		log(INFO, message, null);
	}
	
	public static final void warn(String message) {
		log(WARN, message, null);
	}
	
	public static final void warn(String message, Throwable t) {
		log(WARN, message, t);
	}
	
	public static final void error(String message, Throwable t) {
		log(ERROR, message, t);
	}
	
	public static final void fatal(String message, Throwable t) {
		log(FATAL, message, t);
	}
	
	private static String getCallerInfo() {
		return getClassAndMethod(Thread.currentThread().getStackTrace()[4]);
	}
	
	private static String getClassAndMethod(StackTraceElement ste) {
		String className = ste.getClassName().substring(ste.getClassName().lastIndexOf(".") + 1);
		return className + "." + ste.getMethodName() + "(l:" + ste.getLineNumber() + ") ";
	}

}
